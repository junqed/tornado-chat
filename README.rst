======
README
======

Single-host mode, no async yet, no tests.


#. To start project run in the project directory::

    > ./run.sh

#. To sign up (e.g. with httpie)::

    > http post localhost:8000/signup login=testuser1 password=123456
    > http post localhost:8000/signup login=testuser2 password=654321

#. To sign in (e.g. with httpie)::

    > http post localhost:8000/signin login=testuser1 password=123456
    > http post localhost:8000/signin login=testuser2 password=654321

#. To get online list::

    > http localhost:8000/who

#. For chatting connect to ``ws://localhost:8000/chat?auth-token=*****``. Message format::

    {
        "body": ...,
        "to": ...,
    }

   , where:
        * **body**: string, message text
        * **to**: string, optional (for private messages), a user login

#. Response::

    {
        "body": ...,
        "from": ...,
        "date_created": ...,
        "is_private": ...,
    }

   , where:
        * **body**: string, message text
        * **from**: string, sender login
        * **date_created**: datetime string formatted as ISO 8601
        * **is_private**: bool, is message for all or only for certain user


====
TODO
====
* Add async coroutines
* Add async redis library
* Add pubsub subscription for clients instead a memory storage
