# coding: utf-8

from datetime import datetime
import hashlib
from hmac import compare_digest as compare_hash


class IHasher:
    """
    A small interface for password hashers
    """
    def __call__(self, raw_password):
        """ Generate new password """
        return NotImplemented

    def compare(self, pass_hash, raw_password):
        """ Compare hash and raw password string """
        return NotImplemented


class DefaultPasswordHasher(IHasher):
    """
    Encode password with sha256, not salted.
    """
    def __call__(self, raw_password):
        return hashlib.sha256(raw_password.encode('utf-8')).hexdigest()

    def compare(self, pass_hash, raw_password):
        return compare_hash(self(raw_password), pass_hash)


class ChatUser:
    """
    A user model.
    """
    pass_hasher = DefaultPasswordHasher()  # type: IHasher

    def __init__(self, login, password_hash):
        self.login = login
        self.password_hash = password_hash

    def compare(self, raw_password):
        return self.pass_hasher.compare(self.password_hash, raw_password)

    @classmethod
    def new(cls, login, raw_password):
        return cls(login, cls.pass_hasher(raw_password))

    def __repr__(self):
        return '<ChatUser: %s>' % self.login


class Message:
    """
    A message model.
    """
    def __init__(self, body, sender, to=None, is_private=False):
        self.body = body
        self.sender = sender
        self.to = to
        self.is_private = is_private
        self.date_created = datetime.utcnow()  # type: datetime

    def __repr__(self):
        if len(self.body) > 5:
            return '<Message: %s...>' % self.body[:6]
        else:
            return '<Message: %s>' % self.body


class Room:
    def __init__(self, user, callback):
        self.user = user
        self.callback = callback

    def __repr__(self):
        return self.user.login

