# coding: utf-8

import tornado.ioloop
import tornado.options

from .app import ChatApplication

tornado.options.define('port', default=8000, help='Port for server', type=int)
tornado.options.define('redis_host', default='localhost', help='Redis host', type=str)


def run_loop():
    tornado.options.parse_command_line()

    app = ChatApplication()
    app.listen(tornado.options.options.port)

    tornado.ioloop.IOLoop.current().start()
