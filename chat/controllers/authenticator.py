# coding: utf-8

import uuid

from ..storage import EntityNotFoundError

__all__ = (
    'Authenticator',
    'AuthenticationError',
    'IGenerator',
    'UUIDTokenGenerator',
)


class IGenerator:
    """
    An interface for token generators.
    """
    def __call__(self, *args, **kwargs):
        return NotImplemented


class UUIDTokenGenerator(IGenerator):
    """
    Token generator based on uuid4 algo.
    """
    def __call__(self):
        return uuid.uuid4().hex


class Authenticator:
    token_generator = UUIDTokenGenerator()  # type: IGenerator

    def __init__(self, storage):
        self.storage = storage

    def __call__(self, login, password):
        try:
            user = self.storage.user.find_by_login(login)
        except EntityNotFoundError:
            raise AuthenticationError("User not found")

        if not user.compare(password):
            raise AuthenticationError("Wrong password")

        token = self.token_generator()

        self.storage.session.save(user, token)

        return token


class AuthenticationError(Exception):
    pass
