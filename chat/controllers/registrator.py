# coding: utf-8

from ..models import ChatUser
from ..storage import EntityAlreadyExistsError

__all__ = (
    'Registrator',
    'RegistrationError',
)


class Registrator:
    def __init__(self, storage):
        self.storage = storage

    def __call__(self, login, raw_password):
        """
        Register a new user
        :raise RegistrationError: when user with such login already exists or on invalid data
        """
        self._validate(login, raw_password)

        try:
            self.storage.user.add_new(ChatUser.new(login, raw_password))
        except EntityAlreadyExistsError:
            raise RegistrationError("User with login %s already exists" % login)

    @staticmethod
    def _validate(login, password):
        """
        Validate input data
        :raise RegistrationError: on invalid data
        """
        if len(login) < 3 or len(password) < 3:
            raise RegistrationError("Login or password must be more than 3 symbols")


class RegistrationError(Exception):
    pass
