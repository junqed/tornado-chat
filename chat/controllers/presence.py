# coding: utf-8

__all__ = (
    'Presence',
)


class Presence:
    def __init__(self, storage):
        self.storage = storage

    def who(self):
        return self.storage.get_online_list()
