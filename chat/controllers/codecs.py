# coding: utf-8

import tornado.escape

from ..models import Message
from ..storage import EntityNotFoundError

__all__ = (
    'Codec',
    'CodecParserError',
)


class CodecParserError(Exception):
    pass


class Codec:
    def __init__(self, storage):
        self.storage = storage

    def decode(self, sender, binary):
        """
        Decode sent data to a message
        """
        data = tornado.escape.json_decode(binary)

        try:
            body = data['body'].strip()
        except KeyError:
            raise CodecParserError("No message body")

        if len(body) <= 0:
            raise CodecParserError("Message body can't be empty")

        receiver = None
        if 'to' in data:
            try:
                receiver = self.storage.user.find_by_login(data['to'])
            except EntityNotFoundError:
                raise CodecParserError("Receiver not found")

        return Message(body, sender, receiver)

    def encode(self, message):
        """
        Encode message as bytes.
        """

        payload = {
            'from': message.sender.login,
            'body': message.body,
            'is_private': message.is_private,
            'date_created': message.date_created.isoformat()
        }

        return tornado.escape.json_encode(payload)
