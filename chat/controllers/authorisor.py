# coding: utf-8

from ..storage import EntityNotFoundError

__all__ = (
    'Authorisor',
    'AuthorisationError',
)


class Authorisor:
    def __init__(self, storage):
        self.storage = storage

    def __call__(self, token):
        try:
            user = self.storage.session.get(token)
        except EntityNotFoundError:
            raise AuthorisationError('Can\'t authorise this token')

        return user


class AuthorisationError(Exception):
    pass
