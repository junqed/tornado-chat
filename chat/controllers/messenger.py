# coding: utf-8

__all__ = (
    'Messenger',
)


class Messenger:
    """
    A class to keep all connected users.
    """
    def __init__(self):
        self.rooms = {}

    def on_air(self, room):
        """
        Make a new room for user
        """
        self.rooms.setdefault(str(room), []).append(room.callback)

    def offline(self, room):
        """
        Remove callback from the room if the client has been disconnected.
        Destroy the room if there are now active connections.
        """
        room_name = str(room)

        if room_name in self.rooms:
            self.rooms[room_name].remove(room.callback)

            if len(self.rooms[room_name]) == 0:
                # clean if no callbacks
                del self.rooms[room_name]

    def get_online_list(self):
        """
        Return a list of nicks from all rooms.
        """
        return [room_name for room_name in self.rooms.keys()]

    def send(self, message):
        """
        Send a message to a certain user or to all.
        """
        sender_login = message.sender.login

        if message.to is not None:
            # to a certain user
            to_login = message.to.login

            if to_login == sender_login:
                # don't send to yourself
                return

            try:
                message.is_private = True  # mark as private

                [cb(message) for cb in self.rooms[to_login]]
            except KeyError:
                # no rooms, just ignore
                pass
        else:
            # to all

            for room_name, room_callbacks in self.rooms.items():
                if room_name != sender_login:
                    # don't send to yourself
                    [cb(message) for cb in room_callbacks]
