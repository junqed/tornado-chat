# coding: utf-8

import tornado.web

from .db import create_db_connection
from .handlers import (
    AuthenticationHandler,
    ChatHandler,
    PresenceHandler,
    RegistrationHandler,
)
from .controllers import Messenger


class ChatApplication(tornado.web.Application):
    rooms = None

    _db_conn = None
    _messenger = None

    def __init__(self, *args, **kwargs):
        handlers = [
            (r'/signup', RegistrationHandler),
            (r'/signin', AuthenticationHandler),
            (r'/who', PresenceHandler),
            (r'/chat', ChatHandler),
        ]

        super(ChatApplication, self).__init__(handlers, *args, **kwargs)

        self.messenger = Messenger()

    @property
    def db_conn(self):
        if self._db_conn is None:
            self._db_conn = create_db_connection()

        return self._db_conn

