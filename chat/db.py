# coding: utf-8

import redis
import tornado.options


def create_db_connection():
    """
    Create a new database connection
    :return:
    :rtype redis.StrictRedis:
    """
    return redis.StrictRedis(host=tornado.options.options.redis_host)
