# coding: utf-8

from .models import ChatUser


class EntityAlreadyExistsError(Exception):
    pass


class EntityNotFoundError(Exception):
    pass


class Storage:
    """
    An namespace for storages.
    """
    def __init__(self, conn):
        self.user = UserStorage(conn)
        self.session = SessionStorage(conn, self.user)


class UserStorage:
    def __init__(self, conn):
        self.conn = conn

    def add_new(self, user):
        """
        Add a new user to the storage.
        :raise EntityAlreadyExistsError: when user already in database
        """
        user_key = self._build_key(user)

        if self.conn.hsetnx(user_key, 'login', user.login):
            # if no user record in db
            self.conn.hset(user_key, 'password_hash', user.password_hash)
        else:
            raise EntityAlreadyExistsError('User with login %s' % user.login)

    def find_by_login(self, login):
        data = self._get(login)

        if not data:
            raise EntityNotFoundError("User %s not found" % login)

        return user_data_mapper(data)

    def _get(self, login):
        return self.conn.hgetall('user:%s' % login)

    def _build_key(self, user):
        return 'user:%s' % user.login


class SessionStorage:
    def __init__(self, conn, user_storage):
        self.conn = conn
        self.user = user_storage

    def save(self, user, token):
        self.conn.set('session:%s' % token, user.login)

    def get(self, token):
        """
        Get a user by token.
        :raise EntityNotFoundError: when sent token not found
        """
        session_login = self.conn.get(self._build_key(token))

        if not session_login:
            raise EntityNotFoundError('Session not found')

        return user_data_mapper(self.user._get(session_login.decode('utf-8')))

    def _build_key(self, token: str) -> str:
        return 'session:%s' % token


def user_data_mapper(data):
    login = data[b'login'].decode('utf-8')  # type: str
    password_hash = data[b'password_hash'].decode('utf-8')  # type: str

    return ChatUser(login, password_hash)
