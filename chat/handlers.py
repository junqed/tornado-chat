# coding: utf-8

from functools import wraps
import logging

from tornado.web import (
    HTTPError,
    RequestHandler,
)
from tornado.websocket import WebSocketHandler

from . import controllers as ctrls
from .mixins import (
    JSONErrorFormatterMixin,
    JSONRequestParserMixin,
)
from .models import Room
from .storage import Storage


def json_section_wrapper(section_name='data'):
    """
    Decorator to wrap a json payload with a certain section name
    """
    def decorator(f):

        @wraps(f)
        def wrapper(*args, **kwargs):
            res = f(*args, **kwargs)
            return {section_name: res}

        return wrapper
    return decorator


def auth_data_getter(f):
    """
    Return to the method request parameters for auth/registration modules
    """
    def wrapper(self):
        parsed_data = {}

        if self.is_json():
            try:
                data = self.get_parsed_json()
                parsed_data['login'], parsed_data['password'] = data['login'], data['password']
            except KeyError:
                pass

        else:
            parsed_data['login'], parsed_data['password'] \
                = self.get_argument('login'), self.get_argument('password')

        if parsed_data:
            f(self, **parsed_data)
        else:
            raise HTTPError(400, "Invalid data")

    return wrapper


class RegistrationHandler(JSONRequestParserMixin, JSONErrorFormatterMixin, RequestHandler):
    """
    Endpoint to register a new user.

    Statuses:
        * 400: invalid data
        * 201: success
    """
    registrator = None

    def initialize(self):
        self.registrator = ctrls.Registrator(Storage(self.application.db_conn))

    @auth_data_getter
    def post(self, login, password):
        try:
            self.registrator(login, password)
        except ctrls.RegistrationError:
            raise HTTPError(400, "Can't register user with login %s" % login)

        self.set_status(201)


class AuthenticationHandler(JSONRequestParserMixin, JSONErrorFormatterMixin, RequestHandler):
    """
    Endpoint to authenticate users by theirs credentials.

    Statuses:
        * 401: auth error or invalid data
        * 200: success, return a token
    """
    auth = None

    def initialize(self):
        self.auth = ctrls.Authenticator(Storage(self.application.db_conn))

    @auth_data_getter
    def post(self, login, password):
        try:
            token = self.auth(login, password)
        except (ctrls.AuthenticationError, KeyError):
            raise HTTPError(401)

        self.write(self._create_view(token))

    @json_section_wrapper()
    def _create_view(self, token):
        return {'token': token}


class PresenceHandler(JSONErrorFormatterMixin, RequestHandler):
    """
    Endpoint to get a list of online users.

    Statuses:
        * 200: returns a list of users
    """
    presence = None

    def initialize(self):
        self.presence = ctrls.Presence(self.application.messenger)

    def get(self):
        self.write(self._create_view(self.presence.who()))

    @json_section_wrapper()
    def _create_view(self, users):
        return [{'login': login} for login in users]


class ChatHandler(WebSocketHandler):
    """
    A chat endpoint.
    """
    auth = None
    codec = None
    messenger = None
    room = None

    def initialize(self):
        storage = Storage(self.application.db_conn)

        self.auth = ctrls.Authorisor(storage)
        self.codec = ctrls.Codec(storage)
        self.messenger = self.application.messenger

    def open(self):
        token = self.get_argument('auth-token')

        try:
            user = self.auth(token)
        except ctrls.AuthorisationError:
            logging.info('Authentication failed on socker connection')
            self.send_error(403)
            return

        self._current_user = user
        self.room = Room(self.current_user, self.room_callback)
        self.messenger.on_air(self.room)

        logging.info('New user connected')

    def room_callback(self, message):
        self.write_message(self.codec.encode(message))

        logging.info('Received new message to callback')

    def on_message(self, message_payload):
        logging.info('Received new message')

        try:
            message = self.codec.decode(self.current_user, message_payload)
        except ctrls.CodecParserError:
            # just ignore broken message
            logging.info('Skip broken message')
            return

        self.messenger.send(message)

    def on_close(self):
        if self.current_user is not None:
            self.messenger.offline(self.room)

        logging.info('User disconnected')

    def check_origin(self, _):
        """
        Accept all origins (only for testing purpose)
        :return: always trye
        """
        return True
