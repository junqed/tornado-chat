# coding: utf-8

import traceback

from tornado.escape import json_decode


class JSONRequestParserMixin:
    """
    A small support of json requests.
    """
    def is_json(self):
        """
        Check if the current request with json payload
        """
        return self.request.headers.get('Content-Type') == 'application/json'

    def get_parsed_json(self):
        """
        Parse json request
        """
        return json_decode(self.request.body)



class JSONErrorFormatterMixin:
    """
    Mixin to return all errors as a json payload.
    """
    def write_error(self, status_code, **kwargs):
        self.set_header('Content-Type', 'application/json')

        if self.settings.get("serve_traceback") and "exc_info" in kwargs:
            # in debug mode, try to send a traceback
            error_payload = {
                'code': status_code,
                'error': "".join([line for line in traceback.format_exception(*kwargs["exc_info"])]),
            }

        else:
            error_payload = {
                'code': status_code,
                'error': self._reason,
            }

        self.finish(error_payload)
