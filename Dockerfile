FROM python:3-onbuild
CMD ["python", "run.py", "--redis_host=redis"]
